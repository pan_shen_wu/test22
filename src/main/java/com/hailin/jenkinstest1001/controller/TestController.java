package com.hailin.jenkinstest1001.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Value("${jenkins.param.id}")
    private String param;

    @GetMapping("/gt")
    private String getTest(@RequestParam String id){
        if(StringUtils.isEmpty(id)){
            id = param;
        }
        return "1111111111111111111111111==>>>>>>>>>>>>>>>>>>>====!!!!!!!!!!!!!!!!!!"+id;
    }
}
