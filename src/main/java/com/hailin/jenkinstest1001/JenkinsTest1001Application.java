package com.hailin.jenkinstest1001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsTest1001Application {

    public static void main(String[] args) {
        SpringApplication.run(JenkinsTest1001Application.class, args);
    }

}
